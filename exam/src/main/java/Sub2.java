import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Sub2 extends JFrame {

    private JButton button;
    private TextArea text;

    private static Sub2 instance;

    public static Sub2 getInstance() {
        if (instance == null) {
            instance = new Sub2();
        }
        return instance;
    }

    private Sub2() {
        init();
    }

    public void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 300);
        setLayout(new GridLayout(2, 1));

        button = new JButton();
        button.setText("Random");
        add(button);

        text = new TextArea();
        add(text);

        setVisible(true);
    }

    public TextArea getText() {
        return text;
    }

    public JButton getButton() {
        return button;
    }
}

class ActionButton implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        Sub2 sub2 = Sub2.getInstance();

        Random random = new Random();
        int number = random.nextInt(100);

        sub2.getText().append(number + "\n");
    }
}

class Main {
    public static void main(String[] args) {
        Sub2 sub2 = Sub2.getInstance();
        sub2.getButton().addActionListener(new ActionButton());
    }
}
