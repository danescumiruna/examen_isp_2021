
public class Sub1 {
    public static void main(String[] args) {
    }
}

class K {
}

class A {
    private M m;

    public void metA() {
    }
}

class B {
    public void metB() {
    }
}

class M extends K {
    private B b;

    public M() {
        b = new B();
    }
}

class L {
    private int a;
    private M m;

    public void i(X x) {
    }
}

class X {
}


